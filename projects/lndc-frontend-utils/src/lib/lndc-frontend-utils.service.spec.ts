import { TestBed } from '@angular/core/testing';

import { LndcFrontendUtilsService } from './lndc-frontend-utils.service';

describe('LndcFrontendUtilsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LndcFrontendUtilsService = TestBed.get(LndcFrontendUtilsService);
    expect(service).toBeTruthy();
  });
});
