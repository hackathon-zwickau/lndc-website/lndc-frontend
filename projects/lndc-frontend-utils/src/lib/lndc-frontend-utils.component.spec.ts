import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LndcFrontendUtilsComponent } from './lndc-frontend-utils.component';

describe('LndcFrontendUtilsComponent', () => {
  let component: LndcFrontendUtilsComponent;
  let fixture: ComponentFixture<LndcFrontendUtilsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LndcFrontendUtilsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LndcFrontendUtilsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
