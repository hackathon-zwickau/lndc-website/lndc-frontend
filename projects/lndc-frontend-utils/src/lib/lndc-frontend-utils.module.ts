import { NgModule } from '@angular/core';
import { LndcFrontendUtilsComponent } from './lndc-frontend-utils.component';



@NgModule({
  declarations: [LndcFrontendUtilsComponent],
  imports: [
  ],
  exports: [LndcFrontendUtilsComponent]
})
export class LndcFrontendUtilsModule { }
